// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:social_media/Logic/STM/Language/language.dart';
import 'package:social_media/UI/Screens/home.dart';
import 'package:social_media/UI/Screens/setting.dart';

import 'UI/Screens/Auth/loginScreen.dart';
import 'UI/Screens/addPost.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          foregroundColor: Colors.black,
          centerTitle: true
        ),
        primaryColor: Colors.indigo
      ),

      darkTheme: ThemeData.dark().copyWith(
          appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
            centerTitle: true
    ),
    ),
      themeMode: ThemeMode.light,
      getPages: [
        GetPage(
            name: '/settings',
            page: () => SettingsScreen(),
        ),
        GetPage(
          name: '/home',
          page: () => HomeScreen(),
        ),
        GetPage(
          name: '/login',
          page: () =>LoginScreen(),
        ),
        GetPage(
          name: '/addPost',
          page: () =>AddPostScreen(),
        ),
      ],
      home: LoginScreen(),

      locale: Locale('en','us'),
      translations: Language(),


    );
  }
}
