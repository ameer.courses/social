// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:social_media/Logic/STM/Controllers/settingsController.dart';
import 'package:social_media/UI/Screens/screen.dart';
import 'package:get/get.dart';

class SettingsScreen extends StatelessWidget {

  static final SettingsController controller = Get.put(SettingsController());

  @override
  Widget build(BuildContext context) {
    return Screen(
      title: 'Settings'.tr,
      body: GetBuilder<SettingsController>(
        init: controller,
        builder: (controller) {
          return ListView(
            children: [
              ListTile(
                leading: Icon(controller.theme ? Icons.wb_sunny_outlined :  Icons.nightlight_round),
                title: Text('Theme'),
                subtitle: Text(controller.theme ? 'light' :  'dark'),
                trailing: IconButton(
                  icon: Icon(Icons.change_circle),
                  onPressed: (){
                    controller.changeTheme();
                  },
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.translate),
                title: Text('Language'.tr),
                subtitle: Text('English'.tr),
                trailing: IconButton(
                  icon: Icon(Icons.change_circle),
                  onPressed: (){
                    controller.changeLang();
                  },
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.lock),
                title: Text('Change Password'),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: (){},
                ),
              ),
              Divider(),

            ],
          );
        }
      ),
    );
  }
}
