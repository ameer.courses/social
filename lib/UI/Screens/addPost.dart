// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social_media/Logic/API/Controller/postController.dart';
import 'package:social_media/Logic/API/Models/createPostModel.dart';
import 'package:social_media/UI/Screens/screen.dart';
import 'package:image_picker/image_picker.dart';

class AddPostScreen extends StatefulWidget {
  @override
  State<AddPostScreen> createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  final TextEditingController controller = TextEditingController();
  String? _path;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Screen(
      title: 'Add Post',
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              controller: controller,
              decoration: InputDecoration(
                hintText: 'what are you thinking?'
              ),
            ),
            Container(
              width: double.infinity,
              height: size.height*0.5,
              color: Colors.grey.withOpacity(0.2),
              alignment: Alignment.center,
              child: ( _path == null )
                  ? IconButton(
                       onPressed: () async {
                         _path = await pickImage();
                        setState((){});
                       },
                       icon: Icon(Icons.add_a_photo_outlined),
                    )
                  : Image.file(File(_path!))
              ,
            ),
            ElevatedButton(
                onPressed: () async{
                    print(await PostController.addPost(
                        createPostModel(
                            description: controller.text,
                            image: _path
                        )
                    ));
                    Get.offAllNamed('/home');
                },
                child: Text('Post')
            ),
          ],
        ),
      ),
    );
  }

  Future<String?> pickImage() async{
    ImagePicker picker = ImagePicker();
    XFile? img = await picker.pickImage(source: ImageSource.gallery);

    if(img != null)
      return img.path;

  }
}
