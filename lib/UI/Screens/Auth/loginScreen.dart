// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:social_media/Logic/API/Controller/authController.dart';
import 'package:social_media/Logic/API/Models/userModel.dart';
import 'package:social_media/Logic/STM/Storage/userStorage.dart';
import 'package:social_media/UI/Elements/myTextFields.dart';
import 'package:social_media/UI/Screens/screen.dart';

class LoginScreen extends StatelessWidget {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Screen(
        title: 'Login',
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(size.width*0.02),
                  child: CircleAvatar(
                    radius: size.width*0.25,
                    child: Lottie.network('https://raw.githubusercontent.com/xvrh/lottie-flutter/master/example/assets/Mobilo/A.json'),
                  ),
                ),

                AuthTextField(
                  icon: Icon(Icons.email),
                  hint: 'Email',
                  keyBoard: TextInputType.emailAddress,
                  controller: emailController,
                ),
                AuthTextField(
                  icon: Icon(Icons.lock),
                  hint: 'Password',
                  controller: passController,
                  isPass: true,
                ),

                ElevatedButton(
                    onPressed: () async {
                      UserModel model = await AuthController.login(emailController.text, passController.text);

                      if(model.error != null)
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(model.error!)));
                      else{
                        UserStorage().model = model;
                        await UserStorage().save();
                        Get.offAllNamed('/home');
                     }
                      },
                    child: Text('Login')
                ),

                Padding(
                    padding: EdgeInsets.all(size.width*0.02),
                    child: InkWell(
                      onTap: (){},
                      child: Text('register now.'),
                    ),
                ),
              ],
            ),
          ),

        ),
    );
  }
}
