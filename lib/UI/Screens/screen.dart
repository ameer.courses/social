import 'package:flutter/material.dart';

class Screen extends StatelessWidget {
  final Widget? body;
  final String title;
  final Drawer? drawer;
  final FloatingActionButton? floatingActionButton;

  Screen({this.body,this.drawer,this.floatingActionButton,required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: body,
      drawer: drawer,
      floatingActionButton: floatingActionButton,
    );
  }
}
