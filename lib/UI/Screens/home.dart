// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:social_media/Logic/API/Controller/authController.dart';
import 'package:social_media/Logic/API/Controller/postController.dart';
import 'package:social_media/Logic/API/Models/postModel.dart';
import 'package:social_media/Logic/STM/Storage/userStorage.dart';
import 'package:social_media/UI/Elements/postView.dart';
import 'package:social_media/UI/Screens/screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Screen(
      title: 'Home',
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
                child: Container(
                  color: theme.primaryColor,
                  width: double.infinity,
                  height: double.infinity,
                  padding: EdgeInsets.all(size.width *0.025),
                  child: LayoutBuilder(
                    builder: (context,constrains) {
                      return Row(
                          children: [
                            CircleAvatar(
                              radius: constrains.maxHeight*0.4,
                            ),
                            Expanded(
                              child: ListTile(
                                title: Text(UserStorage().model.name),
                                subtitle: Text(UserStorage().model.email),
                              ),
                            ),
                          ],
                      );
                    }
                  ),
                ),
                padding: EdgeInsets.zero,
            ),
            ListTile(
              title: Text('profile'),
              leading: Icon(Icons.person_outline),
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings_outlined),
              onTap: (){
                Get.toNamed('/settings');
              },
            ),
            ListTile(
              title: Text('Logout'),
              leading: Icon(Icons.logout),
              onTap: ()async{
                await AuthController.logout();
                Get.offAllNamed('/login');
              },
            ),
            Divider(),
            ListTile(
              title: Text('About us'),
              leading: Icon(Icons.info_outlined),
            ),
            ListTile(
              title: Text('Contact us'),
              leading: Icon(Icons.phone_in_talk_outlined),
            ),
            Divider(),
            ListTile(
              title: Text('Report errors'),
              leading: Icon(Icons.report_gmailerrorred_outlined),
            ),

          ],
        ),
      ),
      body: FutureBuilder<List<PostModel>>(
        future: PostController.home(),
        builder: (context,snapShot){
          if(snapShot.hasData )
            return ListView.builder(
              itemCount: snapShot.data!.length,
              itemBuilder: (context,i){
                return PostView(snapShot.data![i]);
              },
            );

          if(snapShot.hasError)
            return Center(child: Text(snapShot.error.toString()),);

          return Center(child: CircularProgressIndicator(),);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Get.toNamed('/addPost');
        },
        child: Icon(Icons.post_add),
      ),
    );
  }
}
