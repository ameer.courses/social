import 'package:flutter/material.dart';

class AuthTextField extends StatefulWidget {
  final String? hint;
  final Widget? icon;
  final TextInputType? keyBoard;
  final TextEditingController? controller;
  final bool isPass;
  bool _hide = true;

  AuthTextField({this.hint, this.icon, this.keyBoard, this.controller,this.isPass = false});

  @override
  State<AuthTextField> createState() => _AuthTextFieldState();
}

class _AuthTextFieldState extends State<AuthTextField> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
        decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.4),
            borderRadius: BorderRadius.circular(size.width*0.025)
        ),
        margin: EdgeInsets.all(size.width*0.025),
        padding: EdgeInsets.all(size.width*0.025),
        child: TextField(
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: widget.hint,
              prefixIcon: widget.icon,
              suffixIcon: widget.isPass ?
                  IconButton(
                      onPressed: (){
                        setState(() {
                          widget._hide = ! widget._hide;
                        });
                      },
                      icon: (widget._hide)?
                      Icon(Icons.visibility_off_outlined)
                          :Icon(Icons.visibility_outlined)
                  )
                  : null,
          ),
          keyboardType: widget.keyBoard,
          controller: widget.controller,
          obscureText: widget._hide && widget.isPass,
        )
    );
  }
}
