import 'package:flutter/material.dart';
import 'package:social_media/Logic/API/Models/postModel.dart';


class PostView extends StatelessWidget {

  final PostModel model;


  PostView(this.model);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height*0.35,
      margin: EdgeInsets.all(size.width*0.025),
      color: Colors.grey.withOpacity(0.4),
      child: Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              foregroundImage: NetworkImage(model.user.image),
            ),
            title: Text(model.user.name),
            subtitle: Text(model.description),
          ),
          Expanded(
            child: GestureDetector(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                    image: (model.image == null) ? null : DecorationImage(
                        image: NetworkImage(model.image!),
                        fit: BoxFit.cover
                    )
                ),
              ),
              onTap: (){
                if(model.image != null)
                showDialog(context: context,
                    builder: (context){
                      return Dialog(
                        insetPadding: EdgeInsets.zero,
                        child:  InteractiveViewer(
                          child: Image.network(model.image!)
                        ),
                      );
                    }
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
