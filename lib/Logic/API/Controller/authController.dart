import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:social_media/Logic/API/Controller/Controller.dart';
import 'package:social_media/Logic/API/Models/userModel.dart';
import 'package:social_media/Logic/STM/Storage/userStorage.dart';

class AuthController extends Controller{


  static Future<UserModel> login(String email,String pass) async {
    var response = await http.post(
        Uri.parse('${Controller.url}/login'),
        body:{
          'email': email,
          'password': pass
        },
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    if(json['error'] == null)
      return UserModel.json(json);

    return UserModel.error(json['error']);

  }

  static Future<String> logout() async {
    var response = await http.get(
        Uri.parse('${Controller.url}/logout'),
        headers: {
          'Authorization': 'Bearer ${UserStorage().model.token}',
          'Accept' : 'application/json'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);
    return json['message'];
  }




}