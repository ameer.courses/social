import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:social_media/Logic/API/Controller/Controller.dart';
import 'package:social_media/Logic/API/Models/createPostModel.dart';
import 'package:social_media/Logic/API/Models/postModel.dart';
import 'package:social_media/Logic/API/Models/userModel.dart';
import 'package:social_media/Logic/STM/Storage/userStorage.dart';


class PostController extends Controller{


  static Future<String> addPost(createPostModel model) async {
    var headers = {
      'Authorization': 'Bearer ${UserStorage().model.token}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${Controller.url}/post/add'));

    // body
    request.fields.addAll(model.toJson);

    // add image
  if(model.image != null)
    request.files.add(await http.MultipartFile.fromPath('image', model.image!));

  //add headers
  request.headers.addAll(headers);


  // send request
    var response = await request.send();

    // response.body
      Map<String,dynamic> json = jsonDecode(await response.stream.bytesToString());

      return json.toString();

  }


  static Future<List<PostModel>> home() async {
  var response = await http.get(Uri.parse('${Controller.url}/home'));
  Map<String,dynamic> json = jsonDecode(response.body);

  List<dynamic> posts = json['posts'];
  List<PostModel> models = [];

  print(posts);

  for(var p in posts)
    models.add(PostModel.fromJson(p));

  return models;
  }

}