

import 'package:social_media/Logic/API/Models/userModel.dart';

class PostModel {

  final String description;
  final String? image;
  final UserModel user;

  PostModel({required this.description, this.image,required this.user});

  factory PostModel.fromJson(Map<String,dynamic> json) =>
      PostModel(description: json['post']['description'],
          image: json['post']['image'],
          user: UserModel.json(json),
      );

}