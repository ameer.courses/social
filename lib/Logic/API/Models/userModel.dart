
class UserModel {

  final String name;
  final String email;
  final String phone;
  final String image;
  final String? token;
  final String? error;

  UserModel({
    required this.name,
    required  this.email,
    required  this.phone,
    required  this.image,
    this.token,
    this.error
  });

  factory UserModel.json(Map<String,dynamic> json) =>
      UserModel(
          name: json['user']['name'],
          email: json['user']['email'],
          phone: json['user']['phone'],
          image: json['user']['image'],
          token: json['token'] ?? ''
      );

  factory UserModel.error(String error) =>
      UserModel(
          name: '',
          email: '',
          phone: '',
          image: '',
          error: error
      );

}