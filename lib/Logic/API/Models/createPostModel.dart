

class createPostModel{
  final String? image;
  final String description;

  createPostModel({
    this.image,
    required this.description
  });

  Map<String,String> get toJson => {
    'description':description
  };
}