import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsController extends GetxController{
  bool _theme = true;
  bool _lang  = true;

  void changeTheme(){
    Get.changeThemeMode( _theme ? ThemeMode.dark : ThemeMode.light);
    _theme = !_theme;
    update();
  }

  void changeLang(){
    Get.updateLocale( _lang ? Locale('ar','sy') :Locale('en','us'));
    _lang = !_lang;
  }

  bool get theme => _theme;
  
}