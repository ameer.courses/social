
import 'package:get_storage/get_storage.dart';
import 'package:social_media/Logic/API/Models/userModel.dart';

class UserStorage{

  late UserModel _model;

  static final UserStorage _instanse = UserStorage._();

  final box = GetStorage();

  UserStorage._(){

    var token = box.read('token');
    if( token != null){
      _model = UserModel(
          name: box.read('name'),
          email: box.read('email'),
          phone: box.read('phone'),
          image: box.read('image'),
          token: token
      );
    }
  }

  factory UserStorage() => _instanse;

  set model(UserModel value) {
    if(value.error == null)
    _model = value;
  }

  UserModel get model => _model;

  Future<void> save()async{
    await box.write('name',_model.name);
    await box.write('token',_model.token);
    await box.write('image',_model.image);
    await box.write('email',_model.email);
    await box.write('phone',_model.phone);

  }
}