import 'package:get/get.dart';

class Language extends Translations {

  @override
  Map<String, Map<String, String>> get keys => {
    'en_us': {
      'Settings':'Settings',
      'English': 'English',
      'Language':'Language'
    },
    'ar_sy': {
      'Settings':'الإعدادات',
      'English' : 'العربية',
      'Language': 'اللغة'
    },

  };
}